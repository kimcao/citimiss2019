/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.billing.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cc.billing.Customer;
import cc.billing.Invoice;
import cc.billing.service.CustomerData;
import cc.billing.service.InvoiceData;
import cc.rest.WebService;
import cc.service.CRUDService;

/**
Unit test for the customer web service.

@author Will Provost
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=CustomerWebServiceTest.Config.class)
@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class CustomerWebServiceTest
{
    /**
    Java configuration to support testing.
    The targets auto-wire CRUD service, so we provide map-backed services.
    */
    @Configuration
    @ComponentScan(basePackages="cc.billing.rest")
    public static class Config
    {
        @Bean
        public CRUDService<Customer> customerData ()
        {
            return new CustomerData ();
        }
        
        @Bean
        public CRUDService<Invoice> invoiceData ()
        {
            return new InvoiceData ();
        }
    }

    @Autowired
    private WebService<Customer> service;
    
    /**
    Test for the get-count operation.
    */
    @Test
    public void testCount ()
        throws Exception
    {
        assertEquals (5, service.getCount ());
    }
    
    /**
    Test for the get-all operation.
    */
    @Test
    public void testGetAll ()
        throws Exception
    {
        assertEquals (5, service.getAll ().size ());
    }
    
    /**
    Test for the get-by-ID operation.
    */
    @Test
    public void testGetOne ()
        throws Exception
    {
        Customer customer = service.getByIDEmbedded (1);
        
        assertEquals ("Ardent Hornblower", customer.getName ());
    }
    
    /**
    Test for the get-by-ID operation with a non-existent ID.
    */
    @Test(expected=CRUDService.NotFoundException.class)
    public void testGetOne_NotFound ()
        throws Exception
    {
        service.getByIDEmbedded (-1);
    }
    
    /**
    Test for adding with generated IDs.
    */
    @Test
    public void testAddWithoutID ()
        throws Exception
    {
        Customer customer = new Customer ();
        customer.setFirstName ("First");
        customer.setLastName ("Last");
        
        service.add (customer);
        
        assertNotNull (service.getByIDEmbedded (6));
    }
    
    /**
    Test for the update operation.
    */
    @Test
    public void testUpdate ()
        throws Exception
    {
        Customer customer = service.getByIDEmbedded (1);
        customer.setFirstName ("First");
        customer.setLastName ("Last");
        
        service.update (customer);
        
        assertEquals ("First", service.getByIDEmbedded (1).getFirstName ());
    }
    
    /**
    Test for the remove-by-ID operation.
    */
    @Test
    public void testRemoveByID ()
        throws Exception
    {
        service.removeByID (4);
        assertEquals (4, service.getCount ());
    }
}
