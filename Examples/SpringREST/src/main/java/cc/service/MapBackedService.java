package cc.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
Implementation based on a map. This might be used for real services at a small
scale; or more likely it will be a mock implementation for testing to be
swapped with a real JPA-based service.

@author Will Provost
*/
public class MapBackedService<T>
    implements CRUDService<T>
{
    private Class<T> type;
    private Map<Integer,T> map = new HashMap<> ();
    private Function<T,Integer> IDGetter;
    private BiConsumer<T,Integer> IDSetter;
    private int maxID;

    /**
    Provide a callback function to help us find an object's ID;
    a JPA provider knows how to do this based on entity metadata,
    but we're not that smart!
    */ 
    public MapBackedService (Class<T> type,
        Function<T,Integer> IDGetter, BiConsumer<T,Integer> IDSetter)
    {
        this.type = type;
        this.IDGetter = IDGetter;
        this.IDSetter = IDSetter;
    }
    
    /**
    Helper method to find an unused key for "generation."
    */
    private int generateKey ()
    {
        return ++maxID;
    }
    
    /**
    Return the number of entries in the map.
    */
    public long getCount ()
    {
        return map.size ();
    }

    /**
    Populate a list with all the values.
    */
    public List<T> getAll ()
    {
        List<T> result = new ArrayList<T> ();
        result.addAll (map.values ());
        return result;
    }

    /**
    Return the value at the given key, or 
    */
    public T getByID (int ID)
        throws NotFoundException
    {
        if (!map.containsKey (ID))
            throw new NotFoundException (type, ID);
        
        return map.get (ID);
    }

    /**
    Adds the given object to the data set, with a generated ID
    that is guaranteed to be unique.
    */
    public T add (T newObject)
    {
        int ID = generateKey ();
        map.put (ID, newObject);
        IDSetter.accept (newObject, ID);

        return newObject;
    }

    /**
    Adds the given object to the list, with given "natural" ID.
    */
    public void add (int ID, T newObject)
        throws ConflictException
    {
        if (map.containsKey (ID))
            throw new ConflictException (newObject, ID);
        
        map.put (ID, newObject);
    }

    /**
    Use the registered callback to find the object's ID, and then
    update the corresponding value in the map.
    */
    public T update (T modifiedObject)
        throws NotFoundException
    {
        int ID = IDGetter.apply (modifiedObject);
        if (!map.containsKey (ID))
            throw new NotFoundException (type, ID);
        
        map.put (ID, modifiedObject);
        return modifiedObject;
    }

    /**

    */
    public void remove (T oldObject)
        throws NotFoundException
    {
        int ID = IDGetter.apply (oldObject);
        if (!map.containsKey (ID))
            throw new NotFoundException (type, ID);
        
        map.remove (ID);
    }

    /**

    */
    public void removeByID (int ID)
        throws NotFoundException
    {
        if (!map.containsKey (ID))
            throw new NotFoundException (type, ID);

        map.remove (ID);
    }
}
