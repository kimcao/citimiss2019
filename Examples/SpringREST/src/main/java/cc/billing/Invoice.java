/*
Copyright 2009-2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.billing;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonView;

import cc.rest.Link;
import cc.rest.WebService;

/**
JavaBean representing a single invoice.

@author Will Provost
*/
public class Invoice
{
    public enum PaymentType { CASH, CHECK, CREDIT_CARD };

    private int ID;
    private Customer customer;
    private int number;
    private Date date = new Date ();
    private double amount;
    private Date paidDate;
    private PaymentType paymentType;
    private Link customerLink;
    
    /**
    Accessor for a link to the invoice, by number.
    */
    @JsonView(WebService.SummaryView.class)
    public Link getLink ()
    {
        return new Link ("/Invoice/Number" + number);
    }

    /**
    Accessor for the object ID.
    */
    @JsonView(WebService.BaseView.class)
    public int getID ()
    {
        return ID;
    }
    
    /**
    Mutator for the object ID.
    */
    public void setID (int newValue)
    {
        ID = newValue;
    }
    
    /**
    Accessor for the customer record.
    */
    @JsonView(WebService.EmbeddedView.class)
    public Customer getCustomer ()
    {
        return customer;
    }
    
    /**
    Accessor for the customer ID.
    */
    @JsonView(WebService.IDView.class)
    public int getCustomerID ()
    {
        return customer.getID ();
    }
    
    /**
    Accessor for the customer link.
    */
    @JsonView(WebService.LinkedView.class)
    public Link getCustomerLink ()
    {
        return customer != null ? customer.getLink () : customerLink;
    }
    
    /**
    Mutator for the customer property.
    */
    public void setCustomer (Customer newValue)
    {
        customer = newValue;
    }
    
    /**
    Mutator for the customer-link HREF only.
    */
    public void setCustomerLink (Link newValue)
    {
        customerLink = newValue;
    }
    
    /**
    Accessor for the number property.
    */
    @JsonView(WebService.BaseView.class)
    public int getNumber ()
    {
        return number;
    }
    
    /**
    Mutator for the number property.
    */
    public void setNumber (int newValue)
    {
        number = newValue;
    }
    
    /**
    Accessor for the date property.
    */
    @JsonView(WebService.BaseView.class)
    public Date getDate ()
    {
        return date;
    }
    
    /**
    Mutator for the date property.
    */
    public void setDate (Date newValue)
    {
        date = newValue;
    }
    
    /**
    Accessor for the amount property.
    */
    @JsonView(WebService.BaseView.class)
    public double getAmount ()
    {
        return amount;
    }
    
    /**
    Mutator for the amount property.
    */
    public void setAmount (double newValue)
    {
        amount = newValue;
    }
    
    /**
    Accessor for the paidDate property.
    */
    @JsonView(WebService.BaseView.class)
    public Date getPaidDate ()
    {
        return paidDate;
    }
    
    /**
    Mutator for the paidDate property.
    */
    public void setPaidDate (Date newValue)
    {
        paidDate = newValue;
    }
    
    /**
    Accessor for the paymentType property.
    */
    @JsonView(WebService.BaseView.class)
    public PaymentType getPaymentType ()
    {
        return paymentType;
    }
    
    /**
    Mutator for the paymentType property.
    */
    public void setPaymentType (PaymentType newValue)
    {
        paymentType = newValue;
    }
}

