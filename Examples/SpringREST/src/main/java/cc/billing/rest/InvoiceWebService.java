/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.billing.rest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cc.billing.Customer;
import cc.billing.Invoice;
import cc.billing.InvoiceService;
import cc.rest.WebService;
import cc.service.CRUDService;

import org.springframework.web.bind.annotation.CrossOrigin;
import com.fasterxml.jackson.annotation.JsonView;

/**
CRUD-style web service for invoice records.

@author Will Provost
*/
@RestController
@RequestMapping("billing/invoices")
@CrossOrigin(origins="*")
public class InvoiceWebService
    extends WebService<Invoice>
{
    /**
    Business exception to express that invoices once paid cannot be adjusted.
    */
    public static class AlreadyPaidException
        extends Exception
    {
        private static DateFormat formatter = new SimpleDateFormat ("M/d/yy");
        private Invoice invoice;

        /**
        Create with a reference to an invoice.
        */
        public AlreadyPaidException (Invoice invoice)
        {
            this.invoice = invoice;
        }

        /**
        Accessor for the related invoice.
        */
        public Invoice getInvoice ()
        {
            return invoice;
        }

        /**
        Accessor for a summary message.
        */
        public String getSummary ()
        {
            return "Can't adjust an invoice that's been paid.";
        }

        /**
        Accessor for a detailed message.
        */
        public String getDetail ()
        {
            return "Invoice " + invoice.getNumber () + " was paid on " +
                formatter.format (invoice.getPaidDate ()) + ".";
        }

        /**
        Accessor for the exception message, overridden here to be a synthesis
        of the {@link #getSummary summary} and {@link #getDetail detail}
        messages.
        */
        @Override
        public String getMessage ()
        {
            return getSummary () + " " + getDetail ();
        }
    }

    /**
    Extended error object for XML or JSON usage: adds a detail field.
    */
    public static class DetailedError
        extends Error
    {
        private String detail;

        /**
        Create with status code, summary and detail messages.
        */
        public DetailedError (int status, String summary, String detail)
        {
            super (status, summary);
            this.detail = detail;
        }

        /**
        Accessor for the detail field.
        */
        public String getDetail ()
        {
            return detail;
        }
    }

    /**
    Returns the associated customer for an invoice, as a sub-resource.

    @throws CRUDService.NotFoundException If no such invoice ID
    */
    @RequestMapping(method=RequestMethod.GET, value="{ID}/Customer")
    @JsonView(BaseView.class)
    public Customer getCustomer (@PathVariable("ID") int ID)
        throws CRUDService.NotFoundException
    {
        return getByIDEmbedded (ID).getCustomer ();
    }

    /**
    Gets the invoice with the given number, or null if not found.

    @throws InvoiceService.NotFoundByNumberException If no such invoice number
    */
    @RequestMapping
    (
        method=RequestMethod.GET,
        value="Number{number}",
        produces={HAL_JSON, MediaType.APPLICATION_JSON_VALUE}
    )
    @JsonView(LinkedView.class)
    public Invoice getByNumber (@PathVariable("number") int number)
        throws InvoiceService.NotFoundByNumberException
    {
        return ((InvoiceService) getDelegate ()).getByNumber (number);
    }

    /**
    Finds the invoice by number and sets the payment type.
    Sets payment date to the current server date and time.

    @throws InvoiceService.NotFoundByNumberException If no such invoice number
    */
    @RequestMapping(method=RequestMethod.PUT, value="Number{number}/Pay")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void payInvoice
    (
        @PathVariable("number")      int invoiceNumber,
        @RequestParam("paymentType") Invoice.PaymentType paymentType
    )
        throws CRUDService.NotFoundException
    {
        Invoice invoice = getByNumber (invoiceNumber);

        invoice.setPaymentType (paymentType);
        invoice.setPaidDate (new Date ());
        update (invoice);
    }

    /**
    Finds the invoice by number and adjusts the amount.

    @throws InvoiceService.NotFoundByNumberException If no such invoice number
    @throws AlreadyPaidException If trying to adjust after invoice has been paid
    */
    @RequestMapping(method=RequestMethod.PUT, value="Number{number}/Adjust")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void adjustInvoice
    (
        @PathVariable("number") int invoiceNumber,
        @RequestParam("amount") double amount
    )
        throws CRUDService.NotFoundException, AlreadyPaidException
    {
        Invoice invoice = getByNumber (invoiceNumber);

        if (invoice.getPaidDate () != null)
            throw new AlreadyPaidException (invoice);

        invoice.setAmount (amount);
        update (invoice);
    }

    /**
    Spring-MVC exception handler for the {@link #AlreadyPaidException}:
    produces a JSON representation of a {@link #DetailedError} object.
    */
    @ExceptionHandler
    public ResponseEntity<DetailedError> handle (AlreadyPaidException ex)
    {
        HttpHeaders headers = new HttpHeaders ();
        headers.add
          ("Content-Type", MediaType.APPLICATION_JSON_VALUE);

        return new ResponseEntity<>
            (new DetailedError (HttpStatus.BAD_REQUEST.value (),
                ex.getSummary (), ex.getDetail ()),
             headers, HttpStatus.BAD_REQUEST);
    }

    /**
    Convenience method to create new invoice and customer records,
    all in one process.
    */
    @RequestMapping(method=RequestMethod.POST, value="CreateForNewCustomer")
    @ResponseStatus(HttpStatus.CREATED)
    public void createForNewCustomer (@RequestBody @Valid Invoice invoice)
    {
        ((InvoiceService) getDelegate ()).addInvoiceAndCustomer (invoice);
    }
}

