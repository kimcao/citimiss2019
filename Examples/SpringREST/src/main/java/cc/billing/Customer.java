/*
Copyright 2005-2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.billing;

import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonView;

import cc.rest.Link;
import cc.rest.WebService;

/**
JavaBean representing a customer.

@author Will Provost
*/
public class Customer
{
    private int ID;
    
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String ZIP;
    
    @Pattern
    (
      regexp="\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*\\.(\\w{2}|(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum))",
      message="Invalid e-mail address"
    )
    private String eMail;

    /**
    Accessor for a link to the invoice, by number.
    */
    @JsonView(WebService.SummaryView.class)
    public Link getLink ()
    {
        return new Link ("/Customer/" + ID);
    }

    /**
    Accessor for the object ID.
    */
    @JsonView(WebService.BaseView.class)
    public int getID ()
    {
        return ID;
    }
    
    /**
    Mutator for the object ID.
    */
    public void setID (int newValue)
    {
        ID = newValue;
    }
    
    /**
    Returns a single, well-formatted string representing the customer's name.
    */
    @JsonView(WebService.SummaryView.class)
    public String getName ()
    {
        return firstName + " " + lastName;
    }
    
    /**
     * @return Returns the firstName.
     */
    @JsonView(WebService.BaseView.class)
    public String getFirstName ()
    {
        return firstName;
    }

    /**
     * @param firstName The firstName to set.
     */
    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    /**
     * @return Returns the lastName.
     */
    @JsonView(WebService.BaseView.class)
    public String getLastName ()
    {
        return lastName;
    }

    /**
     * @param lastName The lastName to set.
     */
    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    /**
     * @return Returns the address1.
     */
    @JsonView(WebService.BaseView.class)
    public String getAddress1 ()
    {
        return address1;
    }

    /**
     * @param address The address to set.
     */
    public void setAddress1 (String address)
    {
        this.address1 = address;
    }

    /**
     * @return Returns the address2.
     */
    @JsonView(WebService.BaseView.class)
    public String getAddress2 ()
    {
        return address2;
    }

    /**
     * @param address The address to set.
     */
    public void setAddress2 (String address)
    {
        this.address2 = address;
    }

    /**
     * @return Returns the city.
     */
    @JsonView(WebService.BaseView.class)
    public String getCity ()
    {
        return city;
    }

    /**
     * @param city The city to set.
     */
    public void setCity (String city)
    {
        this.city = city;
    }

    /**
     * @return Returns the eMail.
     */
    @JsonView(WebService.BaseView.class)
    public String getEMail ()
    {
        return eMail;
    }

    /**
     * @param mail The eMail to set.
     */
    public void setEMail (String mail)
    {
        eMail = mail;
    }

    /**
     * @return Returns the state.
     */
    @JsonView(WebService.BaseView.class)
    public String getState ()
    {
        return state;
    }

    /**
     * @param state The state to set.
     */
    public void setState (String state)
    {
        this.state = state;
    }

    /**
     * @return Returns the ZIP.
     */
    @JsonView(WebService.BaseView.class)
    public String getZIP ()
    {
        return ZIP;
    }

    /**
     * @param code The ZIP to set.
     */
    public void setZIP (String code)
    {
        ZIP = code;
    }
    
    /**
    Equivalence is identity: just compare the ID.
    */
    @Override
    public boolean equals (Object other)
    {
        return (other instanceof Customer)
            ? ((Customer) other).getID () == ID
            : false;
    }
}
