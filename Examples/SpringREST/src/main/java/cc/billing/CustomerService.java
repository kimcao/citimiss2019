/*
Copyright 2009-2010 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.billing;

import cc.service.CRUDService;

/**
CRUD service for Customers.

@author Will Provost
*/
public interface CustomerService
    extends CRUDService<Customer>
{
}

