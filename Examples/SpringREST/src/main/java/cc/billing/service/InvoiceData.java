/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.billing.service;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.billing.Invoice;
import cc.billing.InvoiceService;
import cc.service.CRUDService;
import cc.service.MapBackedService;

/**
CRUD service for invoice records, using hard-coded data.

@author Will Provost
*/
@Component
public class InvoiceData
    extends MapBackedService<Invoice>
    implements InvoiceService
{
    @Autowired
    private CustomerData customers;

    /**
    Provide the logic for getting and setting IDs to the
    superclass constructor.
    */
    public InvoiceData ()
    {
        super (Invoice.class, i -> i.getID(), (i,ID) -> i.setID(ID));
    }

    /**
    Add a series of invoice records.
    */
    @PostConstruct
    public void init ()
        throws CRUDService.NotFoundException
    {
        Date today = new Date ();

        Invoice invoice1 = new Invoice ();
        invoice1.setNumber (123);
        invoice1.setCustomer (customers.getByID (1));
        invoice1.setAmount (1000);
        invoice1.setDate (today);
        add (invoice1);

        Invoice invoice2 = new Invoice ();
        invoice2.setNumber (456);
        invoice2.setCustomer (customers.getByID (2));
        invoice2.setAmount (2000);
        invoice2.setDate (today);
        add (invoice2);

        Invoice invoice3 = new Invoice ();
        invoice3.setNumber (789);
        invoice3.setCustomer (customers.getByID (4));
        invoice3.setAmount (3000);
        invoice3.setDate (today);
        add (invoice3);
    }

    /**
    Gets the invoice with the given number, or null if not found.
    */
    public Invoice getByNumber (int invoiceNumber)
        throws NotFoundByNumberException
    {
        for (Invoice candidate : getAll ())
            if (candidate.getNumber () == invoiceNumber)
                return candidate;

        throw new NotFoundByNumberException (invoiceNumber);
    }
    
    /**
    Adds both the invoice and the customer sub-object 
    to their respective maps.
    */
    public void addInvoiceAndCustomer (Invoice invoice)
    {
        customers.add (invoice.getCustomer ());
        add (invoice);
    }
}

