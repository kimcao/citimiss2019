/*
Copyright 2010-2014 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.rest;

import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
Simple REST service implementation that exercises type conversion.

@author Will Provost
*/
@Controller
@RequestMapping("/Hello")
@CrossOrigin(origins="*")
public class Hello
{
    /**
    Common method called by all service methods.
    */
    private String greet (String name)
    {
        return "Hello, " + name + "!";
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query")
    @ResponseBody
    public String getQuery (@RequestParam("name") String name)
    {
        return greet (name);
    }

    /**
    Greeting method that takes a part of the request path as a parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Path/{name}")
    @ResponseBody
    public String getPath (@PathVariable("name") String name)
    {
        return greet (name);
    }

    /**
    Greeting method that takes an HTTP form parameter.
    */
    @RequestMapping(method=RequestMethod.POST, value="Form")
    @ResponseBody
    public String getForm (@RequestParam("name") String name)
    {
        return greet (name);
    }

    /**
    Greeting method that takes an HTTP header as a parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Header")
    @ResponseBody
    public String getHeader (@RequestHeader("name") String name)
    {
        return greet (name);
    }

    /**
    Greeting method that takes an HTTP cookie as a parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Cookie")
    @ResponseBody
    public String getCookie (@CookieValue("name") String name)
    {
        return greet (name);
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/byte")
    @ResponseBody
    public String getQuery (@RequestParam("number") byte number)
    {
        return greet ("" + number);
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/short")
    @ResponseBody
    public String getQuery (@RequestParam("number") short number)
    {
        return greet ("" + number);
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/int")
    @ResponseBody
    public String getQuery (@RequestParam("number") int number)
    {
        return greet ("" + number);
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/long")
    @ResponseBody
    public String getQuery (@RequestParam("number")  long number)
    {
        return greet ("" + number);
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/float")
    @ResponseBody
    public String getQuery (@RequestParam("number") float number)
    {
        return greet ("" + number);
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/double")
    @ResponseBody
    public String getQuery (@RequestParam("number") double number)
    {
        return greet ("" + number);
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/boolean")
    @ResponseBody
    public String getQuery (@RequestParam("flag")  boolean flag)
    {
        return greet ("" + flag);
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/char")
    @ResponseBody
    public String getQuery (@RequestParam("letter") char letter)
    {
        return greet ("" + letter);
    }

    public enum Answer { YES, NO, MAYBE };

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/enum")
    @ResponseBody
    public String getQuery (@RequestParam("answer") Answer answer)
    {
        return greet ("" + answer);
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/List")
    @ResponseBody
    public String getQuery (@RequestParam("names") List<String> names)
    {
        return greet ("" + names.size () + " people");
    }

    /**
    Greeting method that takes an HTTP query parameter.
    */
    @RequestMapping(method=RequestMethod.GET, value="Query/Set")
    @ResponseBody
    public String getQuery (@RequestParam("names") Set<String> names)
    {
        return greet ("" + names.size () + " unique people");
    }
}
