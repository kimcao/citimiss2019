/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.rest;

import cc.rest.WebService.LinkedView;
import cc.rest.WebService.SummaryView;

import com.fasterxml.jackson.annotation.JsonView;

/**
Model for hyperlinks that can be embedded in data representations --
<u>very loosely</u> modeled after
<a href="http://stateless.co/hal_specification.html" >Hypertext
Application Language</a> but taking only a tiny slice for purposes
of exploring data views and hypermedia.

@author Will Provost
*/
public class Link
{
    private String href;

    /**
    No-arg constructor for de-serialization.
    */
    public Link ()
    {
    }

    /**
    Build with a URL that addresses the related resource.
    */
    public Link (String href)
    {
        this.href = href;
    }

    /**
    Accessor for the link URL.
    */
    @JsonView({SummaryView.class, LinkedView.class})
    public String getHref ()
    {
        return href;
    }

    /**
    Mutator for the link URL.
    */
    public void setHref (String href)
    {
        this.href = href;
    }
}
