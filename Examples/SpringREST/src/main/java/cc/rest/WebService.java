/*
Copyright 2010-2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.annotation.JsonView;

import cc.service.CRUDService;

/**
Generic RESTful service that acts as a proxy to a generic
{@link cc.service.CRUDService CRUD service}, annotating many methods
for HTTP access.

@author Will Provost
*/
public class WebService<T>
{
    public interface SummaryView {}
    public interface BaseView {}
    public interface EmbeddedView extends BaseView {}
    public interface IDView extends BaseView {}
    public interface LinkedView extends BaseView {}

    public static final String HAL_JSON = "application/hal+json";

    /**
    Transfer object to represent error conditions in XML or JSON formats.
    Guided by the JSON API specification for error objects, but only a
    tiny slice of that specification.
    */
    public static class Error
    {
        private int status;
        private String title;
        
        /**
        Build with an HTTP response code and a message or "title."
        */
        public Error (int status, String title)
        {
            this.status = status;
            this.title = title;
        }
        
        /**
        Accessor for the HTTP response code.
        */
        public int getStatus ()
        {
            return status;
        }
        
        /**
        Accessor for the message/title.
        */
        public String getTitle ()
        {
            return title;
        }
        
        /**
        Factory for a Spring response entity that wraps an instance of
        this class, with a JSON content type and the given code and message.
        */
        public static ResponseEntity<Error> produceResponse
            (HttpStatus httpStatus, Throwable ex)
        {
            HttpHeaders headers = new HttpHeaders ();
            headers.add 
              ("Content-Type", MediaType.APPLICATION_JSON_VALUE);
            
            return new ResponseEntity<> 
                (new Error (httpStatus.value (), ex.getMessage ()), 
                    headers, httpStatus);
        }
    }
    
    @Autowired
    private CRUDService<T> delegate;

    /**
    Accessor for direct reference to the delegate service.
    */
    public CRUDService<T> getDelegate ()
    {
        return delegate;
    }

    /**
    Gets all records of this type.
    */
    @RequestMapping
    (
        method=RequestMethod.GET,
        produces={HAL_JSON, MediaType.APPLICATION_JSON_VALUE}
    )
    @JsonView(SummaryView.class)
    public List<T> getAll ()
    {
        return delegate.getAll ();
    }

    /**
    Gets a count of all records of this type.
    */
    @RequestMapping(method=RequestMethod.GET, value="Count")
    public long getCount ()
    {
        return delegate.getCount ();
    }

    /**
    Returns the entity with the given ID.

    @throws CRUDService.NotFoundException If no such ID
    */
    @RequestMapping
    (
        method=RequestMethod.GET,
        value="/{ID}",
        produces=MediaType.APPLICATION_JSON_VALUE
    )
    @JsonView(EmbeddedView.class)
    public T getByIDEmbedded (@PathVariable("ID") int ID)
        throws CRUDService.NotFoundException
    {
        return delegate.getByID (ID);
    }

    /**
    Returns the entity with the given ID, or null if not found.

    @throws CRUDService.NotFoundException If no such ID
    */
    @RequestMapping
    (
        method=RequestMethod.GET,
        value="/{ID}",
        produces=HAL_JSON
    )
    @JsonView(LinkedView.class)
    public T getByIDLinked (@PathVariable("ID") int ID)
        throws CRUDService.NotFoundException
    {
        return delegate.getByID (ID);
    }

    /**
    Adds the given object to the database, with a generated ID.
    */
    @RequestMapping(method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @JsonView(IDView.class)
    public T add (@RequestBody @Valid T newObject)
    {
        return delegate.add (newObject);
    }

    /**
    Merges the given object into the database.

    @throws CRUDService.NotFoundException If no such ID
    */
    @RequestMapping(method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update (@RequestBody @Valid T modifiedObject)
        throws CRUDService.NotFoundException
    {
        delegate.update (modifiedObject);
    }

    /**
    Removes the object with the given ID from the database.

    @throws CRUDService.NotFoundException If no such ID
    */
    @RequestMapping(method=RequestMethod.DELETE, value="/{ID}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeByID (@PathVariable("ID") int ID)
        throws CRUDService.NotFoundException
    {
        delegate.removeByID (ID);
    }

    /**
    Spring-MVC exception handler for the {@link CRUDService#NotFoundException}
    -- calls {@link #produceResponse produceResponse} with HTTP 404.
    */
    @ExceptionHandler
    public ResponseEntity<Error> handle 
        (CRUDService.NotFoundException ex)
    {
        return Error.produceResponse (HttpStatus.NOT_FOUND, ex);
    }

    /**
    Spring-MVC exception handler for the {@link CRUDService#NotFoundException}
    -- calls {@link #produceResponse produceResponse} with HTTP 404.
    */
    @ExceptionHandler()
    public ResponseEntity<Error> handleException
        (CRUDService.ConflictException ex)
    {
        return Error.produceResponse (HttpStatus.CONFLICT, ex);
    }

    /**
    Spring-MVC exception handler for the UnsupportedOperationExceptino
    -- calls {@link #produceResponse produceResponse} with HTTP 501.
    */
    @ExceptionHandler()
    public ResponseEntity<Error> handleException
        (UnsupportedOperationException ex)
    {
        return Error.produceResponse (HttpStatus.NOT_IMPLEMENTED, ex);
    }
}
