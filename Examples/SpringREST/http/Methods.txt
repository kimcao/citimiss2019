============================================================
Test script for DB service.
-- ECHO ON
------------------------------------------------------------
GET /Basic/Number/F HTTP/1.1
============================================================
-- Tweak value of "F", and check it:
--
PUT /Basic/Number/F HTTP/1.1
Content-Type: text/plain

100
--
GET /Basic/Number/F HTTP/1.1
============================================================
-- Put value of "F" back, and check it:
--
PUT /Basic/Number/F HTTP/1.1
Content-Type: text/plain

6
--
GET /Basic/Number/F HTTP/1.1
============================================================
-- Add a key "K", and check it:
--
POST /Basic/Number/K HTTP/1.1
Content-Type: text/plain

11
--
GET /Basic/Number/K HTTP/1.1
--
-- Add it again: should fail
--
POST /Basic/Number/K HTTP/1.1
Content-Type: text/plain

11
============================================================
-- Remove key "K", and check it:
--
DELETE /Basic/Number/K HTTP/1.1
--
GET /Basic/Number/K HTTP/1.1
============================================================
