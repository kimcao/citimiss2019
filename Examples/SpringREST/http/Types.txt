============================================================
Test script for Hello service.
------------------------------------------------------------
GET /Basic/Hello/Query/byte?number=42 HTTP/1.1
------------------------------------------------------------
GET /Basic/Hello/Query/short?number=42 HTTP/1.1
------------------------------------------------------------
GET /Basic/Hello/Query/int?number=42 HTTP/1.1
------------------------------------------------------------
GET /Basic/Hello/Query/long?number=42 HTTP/1.1
------------------------------------------------------------
GET /Basic/Hello/Query/float?number=4.2 HTTP/1.1
------------------------------------------------------------
GET /Basic/Hello/Query/double?number=4.2 HTTP/1.1
------------------------------------------------------------
GET /Basic/Hello/Query/boolean?flag=true HTTP/1.1
------------------------------------------------------------
GET /Basic/Hello/Query/char?letter=Q HTTP/1.1
------------------------------------------------------------
GET /Basic/Hello/Query/enum?answer=MAYBE HTTP/1.1
------------------------------------------------------------
GET /Basic/Hello/Query/List?names=Abbot&names=Abbot&names=Costello HTTP/1.1
------------------------------------------------------------
GET /Basic/Hello/Query/Set?names=Abbot&names=Abbot&names=Costello HTTP/1.1
