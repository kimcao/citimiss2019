package com.citi.recipe.spring;

/**
* Represents any player.
*
* @author Will Provost
*/
public interface Player {

	/**
	 * Prompts the player to move in turn.
	 */
	public void move();
}
