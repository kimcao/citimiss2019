package com.citi.recipe.springdata;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Spring Data repository that extends stock CRUD operations with
 * a couple of type-specific queries.
 *
 * @author Will Provost
 */
public interface BowlerRepository extends CrudRepository<Bowler,Integer> {

	/**
	 * Here we rely on Spring Data's naming conventions to add a query
	 * for a single object by a non-key field -- the bowler's name.
	 * Spring Data figures out the implementation.
	 */
	public Bowler findByName(String name);

	/**
	 * This duplicates the logic of {@link #findByName findByName()},
	 * but we provide the JPQL explicitly.
	 */
	@Query("select b from Bowler b where b.name=:name")
	public Bowler findBowlerWithName(String name);
}
