package com.citi.recipe.springdata;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.recipe.springdata.Bowler;
import com.citi.recipe.springdata.BowlerRepository;

/**
 * Base for test in different environments; supplies test methods,
 * constants, and a helper to prime the database.
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
public class BowlerRepositoryTestBase {

    public static int VALUE_ID = 1;
    public static final String VALUE_NAME = "Fred Farmer";
    public static final int VALUE_HIGH_SCORE = 207;
    public static final Date VALUE_GAME_DATE = new Date(1447822800000L); // 2015-11-18

    public static final String PARAM_ID = "ID";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_HIGH_SCORE = "highScore";
    public static final String PARAM_GAME_DATE = "gameDate";

    @Autowired
    protected BowlerRepository repo;

	@Autowired
	private EntityManagerFactory emf;

	@Autowired
	private DataSource dataSource;

	/**
     * Re-set the test database with prepared rows.
     */
    @Before
    public void primeDatabase() throws Exception {

    	SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd");

    	EntityManager em = emf.createEntityManager();
    	em.getTransaction().begin();

    	em.createQuery("delete from Bowler").executeUpdate();

    	Bowler bowler1 = new Bowler();
    	bowler1.setName("Fred Farmer");
        bowler1.setHighScore(207);
        bowler1.setGameDate(parser.parse("2015-11-18"));
        em.persist(bowler1);
        VALUE_ID = bowler1.getID();
        System.out.println("First ID is " + VALUE_ID);

    	Bowler bowler2 = new Bowler();
    	bowler2.setName("Gloria Grassley");
    	bowler2.setHighScore(241);
    	bowler2.setGameDate(parser.parse("2014-01-25"));
        em.persist(bowler2);

    	Bowler bowler3 = new Bowler();
    	bowler3.setName("Harold Hicks");
    	bowler3.setHighScore(198);
    	bowler3.setGameDate(parser.parse("2015-03-07"));
        em.persist(bowler3);

        em.getTransaction().commit();
        em.close();
    }

    /**
     * Queries the database for a specific bowler, and
     * then checks the values in the returned {@link Bowler}.
     */
    @Test
    public void testGetBowlerByID() throws Exception {

        Bowler result = repo.findById(VALUE_ID).get();

        assertThat(result, notNullValue());
        assertThat(result.getID(), equalTo(VALUE_ID));
        assertThat(result.getName(), equalTo(VALUE_NAME));
        assertThat(result.getHighScore(), equalTo(VALUE_HIGH_SCORE));
        assertThat(result.getGameDate(), equalTo(VALUE_GAME_DATE));
    }

    /**
     * Prepares a {@link Bowler instance} and calls
     * {@link BowlingDAO#recordHighScore(Bowler)}.
     * Opens a JDBC connection to the DB and checks the update.
     * Then runs a second update to put the score back.
     */
    @Test
    public void testUpdateHighScore() throws Exception {

        Bowler bowler = new Bowler();
        bowler.setID(VALUE_ID);
        bowler.setName(VALUE_NAME);
        bowler.setHighScore(VALUE_HIGH_SCORE + 1);
        bowler.setGameDate(VALUE_GAME_DATE);
        repo.save(bowler);

        try (
        	Connection conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery
                    ("SELECT highScore FROM bowler WHERE ID = " + VALUE_ID);
        ) {
            assertThat(rs.next(), equalTo(true));
            assertThat(rs.getInt(PARAM_HIGH_SCORE), equalTo(VALUE_HIGH_SCORE + 1));
        }

        bowler.setHighScore(VALUE_HIGH_SCORE);
        repo.save(bowler);
    }

    /**
     * Prepares a {@link Bowler instance} and calls
     * {@link BowlingDAO#addBowler(Bowler)}.
     * Opens a JDBC connection to the DB and checks the update,
     * and to clean up the row.
     */
    @Test
    public void testAddBowler() throws Exception {

        Bowler bowler = new Bowler();
        bowler.setName(VALUE_NAME);
        bowler.setHighScore(VALUE_HIGH_SCORE);
        bowler.setGameDate(VALUE_GAME_DATE);
        repo.save(bowler);

        try (
        	Connection conn = dataSource.getConnection();
        	Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery
                    ("SELECT * FROM bowler WHERE ID = " + (VALUE_ID + 3));
        ) {
            assertThat(rs.next(), equalTo(true));
            assertThat(rs.getInt(PARAM_ID), equalTo(VALUE_ID + 3));
            assertThat(rs.getString(PARAM_NAME), equalTo(VALUE_NAME));
            assertThat(rs.getInt(PARAM_HIGH_SCORE), equalTo(VALUE_HIGH_SCORE));
            assertThat(rs.getDate(PARAM_GAME_DATE), equalTo(VALUE_GAME_DATE));
        }
    }
}
