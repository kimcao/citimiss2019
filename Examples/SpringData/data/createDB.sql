CREATE SCHEMA SpringData;
SET SCHEMA SpringData;

CREATE TABLE bowler 
(
  ID INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  name VARCHAR(64) NOT NULL,
  highScore INTEGER NOT NULL,
  gameDate DATE NOT NULL
);

INSERT INTO bowler (name, highScore, gameDate) 
  VALUES ('Fred Farmer', 207, '2015-11-18');
INSERT INTO bowler (name, highScore, gameDate) 
  VALUES ('Gloria Grassley', 241, '2014-01-25');
INSERT INTO bowler (name, highScore, gameDate) 
  VALUES ('Harold Hicks', 198, '2015-03-07');
