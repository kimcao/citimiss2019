package com.citi.recipe.mq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.recipe.springjms.OneWaySender;

/**
 * Unit test for the {@link OneWaySender} class.
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=OneWaySenderTest.Config.class)
public class OneWaySenderTest {

	/**
	 * Test configuration, with a Mockito-mocked <strong>JmsTemplate</strong>
	 * and the component under test.
	 */
	@Configuration
	public static class Config {

		@Bean
		public JmsTemplate jmsTemplate() {
			return Mockito.mock(JmsTemplate.class);
		}

		@Bean
		public OneWaySender oneWaySender() {
			return new OneWaySender();
		}
	}

	@Autowired
	public OneWaySender sender;

	@Autowired
	public JmsTemplate mockTemplate;

    /**
     * Ask the sender to send one message to its outbound queue,
     * and verify that it does so.
     */
    @Test
    public void testWrite() {
        sender.send("Test message");
        Mockito.verify(mockTemplate).convertAndSend("OneWayQueue", "Test message");
    }
}
