package com.citi.recipe.mq;

import org.junit.Test;

import com.citi.recipe.springjms.OneWayListener;

/**
 * Unit test for the {@link OneWayListener} class.
 * We don't bother with the Spring test environment here, because
 * there are no outbound dependencies that would call for mocks,
 * and the behavior can be driven with a simple method call
 * on a new object.
 *
 * @author Will Provost
 */
public class OneWayListenerTest {

    /**
     * "Send" the listener a message. There's not much we can check
     * in the way of outcomes here, but a typical component would react
     * to the message by doing some other, testable thing; typically
     * we'd either check for outcomes in a database or we would
     * mock that external thing and verify that appropriate calls
     * were made upon it.
     */
    @Test
    public void testOnMessage() throws Exception {

        OneWayListener listener = new OneWayListener();
        listener.onMessage("Test message");
    }
}
