package com.citi.recipe.mq;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.recipe.springjms.Constants;
import com.citi.recipe.springjms.TwoWaySender;

/**
 * Unit test for the {@link TwoWaySender} class.
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TwoWaySenderTest.Config.class)
public class TwoWaySenderTest {

	/**
	 * Test configuration, with a Mockito-mocked <strong>JmsTemplate</strong>
	 * and the component under test.
	 */
	@Configuration
	public static class Config {

		/**
		 * The mock template will be used both for sending and receiving.
		 * The first requires no stubbing; for the second purpose we stub
		 * a hard-coded reply message.
		 */
		@Bean
		public JmsTemplate jmsTemplate() {
			JmsTemplate mockTemplate = Mockito.mock(JmsTemplate.class);
			Mockito.when(mockTemplate.receiveAndConvert(Constants.TWO_WAY_REPLY_QUEUE))
			.thenReturn("REPLY");
			return mockTemplate;
		}

		@Bean
		public TwoWaySender twoWaySender() {
			return new TwoWaySender();
		}
	}

	@Autowired
	public TwoWaySender sender;

    /**
     * Ask the sender to send one message to its outbound queue,
     * and confirm that it gets the prepared reply.
     */
    @Test
    public void testWrite() {
        assertThat(sender.send("Test message"), is("REPLY"));
    }
}
