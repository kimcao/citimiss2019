package com.citi.recipe.springjms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

/**
 * Component that uses an injected <strong>JmsTemplate</strong> to send
 * text messages to a JMS queue, and then to look for a reply on a
 * separate queue.
 *
 * @author Will Provost
 */
@Component
public class TwoWaySender {

	@Autowired
	private JmsTemplate jmsTemplate;

	/**
	 * Send a text message with the given text.
	 * Look to receive a message on a separate queue, unpack the contents,
	 * and return them to our caller.
	 */
	public String send(String text) {
		jmsTemplate.convertAndSend(Constants.TWO_WAY_REQUEST_QUEUE, text);
		return (String) jmsTemplate.receiveAndConvert(Constants.TWO_WAY_REPLY_QUEUE);
    }
}
