package com.citi.recipe.springjms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

/**
 * Component that uses an injected <strong>JmsTemplate</strong> to send
 * text messages to a JMS queue.
 *
 * @author Will Provost
 */
@Component
public class OneWaySender {

	@Autowired
	private JmsTemplate jmsTemplate;

	/**
	 * Send a text message with the given text.
	 */
	public void send(String text) {
		jmsTemplate.convertAndSend(Constants.ONE_WAY_QUEUE, text);
    }
}
