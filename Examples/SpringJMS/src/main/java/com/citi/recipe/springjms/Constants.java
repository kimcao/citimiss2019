package com.citi.recipe.springjms;

/**
 * We centralize a few property names in this class, to be shared by
 * various senders and listener classes.
 *
 * @author Will Provost
 *
 */
public class Constants {
    public static final String ONE_WAY_QUEUE = "OneWayQueue";
    public static final String TWO_WAY_REQUEST_QUEUE = "TwoWayQueue";
    public static final String TWO_WAY_REPLY_QUEUE = "TwoWayReplyQueue";
}
