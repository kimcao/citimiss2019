package com.citi.recipe.springjms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.JmsListenerEndpointRegistry;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.stereotype.Component;

/**
 * JMS listener that receives and logs messages.
 *
 * @author Will Provost
 */
@Component
public class OneWayListener {

	@Autowired
	private ApplicationContext context;

	/**
	 * Helper method to shut down the surrounding listener container
	 * and Java process.
	 */
	private void shutdown() {

		// Can't @Autowire the registry -- you'll get a
    	// permanently-empty one. Instead, inject the context,
    	// and look it up when needed:
    	JmsListenerEndpointRegistry registry =
    			context.getBean(JmsListenerEndpointRegistry.class);
		for (MessageListenerContainer container : registry.getListenerContainers()) {
			DefaultMessageListenerContainer def = (DefaultMessageListenerContainer) container;
			def.shutdown();
			System.out.println("Stopped listener container.");
		}

		// That gives us a clean disengagement from the JMS queue;
		// we still have to shut down the Java process:
		try { Thread.sleep(5000); } catch (InterruptedException ex) {}
		System.exit(0);
	}

	/**
	 * The listener method will be recognized by Spring-JMS auto-configuration.
	 * It just prints the contents of the expected text message.
	 * If the message is "shutdown" (case-insensitive) it will shut
	 * down as a service.
	 */
	@JmsListener(destination=Constants.ONE_WAY_QUEUE)
    public void onMessage(String text) {
        System.out.println("Received message: " + text);

        if (text.equalsIgnoreCase("SHUTDOWN")) {
        	new Thread(this::shutdown).start();
        }
    }
}
