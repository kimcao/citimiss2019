package com.citi.recipe.springjms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.annotation.EnableJms;

/**
 * Spring Boot application that auto-configures a single
 * {@link TwoWayListener JMS listener}.
 *
 * @author Will Provost
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(excludeFilters={
	@ComponentScan.Filter(type=FilterType.REGEX, pattern=".*Sender.*"),
	@ComponentScan.Filter(type=FilterType.REGEX, pattern=".*OneWay.*")
})
@PropertySource("classpath:application.properties")
@EnableJms
public class TwoWayListenerApplication {

	/**
	 * Run the application as configured.
	 */
	public static void main(String[] args) {
		SpringApplication.run(TwoWayListenerApplication.class, args);
	}
}
