/*
Copyright 2016 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static cc.test.FunctionalMatcher.*;
import static cc.test.PerfectSquare.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

/**
Test class that exercises built-in Hamcrest matchers,
and a couple of custom matchers.

@author Will Provost
*/
public class Hamcrest
{
    /**
    Whip up a bunch of primitives, strings, and JavaBean instances,
    and assert various things about them.
    */
    @Test
    public void testHamcrestMatchers() throws Exception
    {
        String goodBoy = "Every good boy does fine.";

        assertThat (goodBoy, isA (String.class));
        assertThat (goodBoy, startsWith ("Ev"));
        assertThat (goodBoy, containsString ("boy"));
        assertThat (goodBoy, endsWith ("fine."));
        assertThat (goodBoy,
            both (startsWith ("Every")).and (endsWith (".")));
        assertThat (goodBoy,
            either (isEmptyString ()).or (containsString ("boy")));

        Date earlier = new Date (1471001168000L);
        Date later = new Date (1471001169000L);
        assertThat (earlier, lessThan (later));

        List<Integer> numbers = new ArrayList<>();
        Collections.addAll (numbers, 5, 4, 3, 2, 1);

        assertThat (numbers, hasSize (5));
        assertThat (numbers, hasItem (is (5)));
        assertThat (numbers, everyItem (greaterThan (0)));
        assertThat (numbers, everyItem (lessThan (10)));
        assertThat (numbers, containsInAnyOrder (1, 2, 3, 4, 5));
        assertThat (numbers, anyOf (hasItem (1), hasItem (-1)));

        Map<String, Double> probabilities = new HashMap<>();
        probabilities.put ("Heads", .5001);
        probabilities.put ("Tails", .4999);

        assertThat (probabilities, hasKey ("Heads"));
        assertThat (probabilities, hasValue (.5001));
        assertThat (probabilities.get ("Heads") +
            probabilities.get ("Tails"), closeTo (1.00, 0.0001));

        MyBean expected = new MyBean ("one", 1);
        MyBean actual = new MyBean ("one", 1);
        assertThat (actual, samePropertyValuesAs (expected));

        MyBean firstDelegate = new MyBean ("delegate", 1000);
        MyBean first = new MyBean ("main", 100);
        first.setDelegate (firstDelegate);

        // MyBean secondDelegate = new MyBean ("delegate", 1000);
        MyBean second = new MyBean ("main", 100);
        second.setDelegate (firstDelegate); // secondDelegate won't work

        assertThat (first, is (second));
        assertThat (first, samePropertyValuesAs (second));

        assertThat (9, perfectSquare ());

        assertThat (100, exhibits
            ((v) -> v > 99 && v < 1000, "three-digit number"));
        assertThat (new Date (259200000000L), exhibits
            ((Date v) -> v.getTime () % 86400000 == 0, 
                "midnight on any date"));
    }
}
