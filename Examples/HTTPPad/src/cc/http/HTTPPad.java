/*
Copyright 2002-2014 by Capstone Courseware, LLC, and William W. Provost.
All rights reserved.
*/

package cc.http;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import cc.Footer;

/**
Main window and application class for HTTPPad tool.
Allows the user to direct raw HTTP requests to web services and view the
responses. Full content -- header and body -- are entered in the upper
text area; but content-length is adjusted automatically or provided if absent.
The response is shown in the lower text area.

@author Will Provost
*/
public class HTTPPad
    extends JFrame
{
    private HTTPPanel pnHTML;
    private String responseFormat;

    static String FONT_SIZE = System.getProperty
        ("cc.http.HTTPPad.fontSize", "12");
    static String FONT_WEIGHT = System.getProperty
        ("cc.http.HTTPPad.fontWeight", "Normal");
    static
    {
        if (FONT_SIZE.equals (""))
            FONT_SIZE = "12";
        if (FONT_WEIGHT.equals (""))
            FONT_WEIGHT = "Normal";
    }

    /**
    Builds GUI and connects event handlers.
    */
    public HTTPPad (String fontSize, String fontWeight)
    {
        super ("HTTPPad");
        setIconImage (new ImageIcon (HTTPPad.class.getResource
                ("CCIcon.gif")).getImage ());

        setSize (new Dimension (400, 540));
        boolean loaded = load ();
        if (fontSize != null)
            FONT_SIZE = fontSize;
        if (fontWeight != null)
            FONT_WEIGHT = fontWeight;
        if (!loaded || fontSize != null)
        {
            int size = Integer.parseInt (FONT_SIZE);
            setSize (120 + size * 30, 200 + size * 35);
        }

        pnHTML = new HTTPPanel ();
        pnHTML.setFormat (responseFormat);

        getContentPane ().setLayout (new BorderLayout (4, 4));
        getContentPane ().add (BorderLayout.CENTER, pnHTML);
        getContentPane ().add (BorderLayout.SOUTH, new Footer ());

        addWindowListener (new java.awt.event.WindowAdapter ()
            {
                public void windowClosing (java.awt.event.WindowEvent ev)
                {
                    save ();
                    System.exit (0);
                }
            } );

    }

    /**
    Builds GUI as usual; then populates the request area with the given
    string, and triggers a roundtrip -- equivalent to entering a request
    by handing and clicking <strong>Send</strong>.
    */
    public HTTPPad
        (String fontSize, String fontWeight, String root, String request)
    {
        this (fontSize, fontWeight);
        pnHTML.setRoot (root);
        pnHTML.setRequest (request);
        pnHTML.doRequestResponse ();
    }

    /**
    Best-effort utility to load window geometry and font size.
    */
    protected boolean load ()
    {
        try
        (
            BufferedReader in = new BufferedReader
                (new FileReader ("../HTTPPad/HTTPPadInit.txt"));
        )
        {
            setLocation (new Point
                (Integer.parseInt (in.readLine ()),
                 Integer.parseInt (in.readLine ())));
            setSize (new Dimension
                (Integer.parseInt (in.readLine ()),
                 Integer.parseInt (in.readLine ())));

            FONT_SIZE = in.readLine ();
            FONT_WEIGHT = in.readLine ();

            responseFormat = in.readLine ();
        }
        catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    /**
    Saves values for window geometry and font size
    in a text file in the working directory.
    */
    protected void save ()
    {
        try
        (
            PrintWriter out = new PrintWriter
                (new FileWriter ("../HTTPPad/HTTPPadInit.txt"));
        )
        {
            out.println (getLocation ().x);
            out.println (getLocation ().y);
            out.println (getSize ().width);
            out.println (getSize ().height);
            out.println (FONT_SIZE);
            out.println (FONT_WEIGHT);
            out.println (pnHTML.getFormat ());
        }
        catch (IOException ex) {}
    }

    /**
    Three ways to invoke the application:
    <ul>
      <li>No arguments: shows the main window and user works from there</li>
      <li>One argument: takes this as a filename, runs the request
          or script found in that file, and produces all response
          content to the console.</li>
      <li>Two arguments, the first one being the switch "-show":
          takes the second argument as the filename, loads the request
          or script found in that file, creates the main window and
          populates its request area with that content, and triggers
          the HTTP roundtrip(s) so that the user can see the results
          and also tweak the request and try again. Also useful for
          seeing responses in the HTML or XML views provided by
          {@link HTTPPanel}.</li>
    </ul>
    */
    public static void main (String[] args)
    {
        if (args.length != 0 && args[0].equals ("-help"))
        {
            System.out.println ("Usage: java cc.http.HTTPPad ");
            System.out.println
                ("  [-show] [-font:<size>,<weight>] [<request-or-script>]");
            System.exit (0);
        }

        try
        {
            boolean show = false;
            boolean flags = true;
            String fontSize = null;
            String fontWeight = null;
            String root = null;
            Reader file = null;

            for (String arg : args)
            {
                if (flags)
                {
                    if (arg.charAt (0) == '-')
                    {
                        if (arg.equals ("-show"))
                            show = true;
                        else if (arg.startsWith ("-font"))
                        {
                            String[] params =
                                (arg.substring ("-font:".length ()))
                                    .split (",");
                            fontSize = params[0];
                            if (params.length > 1)
                                fontWeight = params[1];
                        }
                        else if (arg.startsWith ("-root"))
                            root = arg.substring ("-root:".length ());
                    }
                    else
                        flags = false;
                }

                if (!flags && file == null)
                    file = new FileReader (arg);
            }

            if (file != null && show)
            {
                BufferedReader in = new BufferedReader (file);
                String line;
                StringBuilder request = new StringBuilder ();
                while ((line = in.readLine ()) != null)
                    request.append (line).append ('\n');
                in.close ();

                new HTTPPad (fontSize, fontWeight, root, request.toString ())
                    .setVisible (true);
            }
            else if (file != null)
            {
                System.out.println  (new HTTPClient (root).invokeScript (file));
            }
            else
            {
                new HTTPPad (fontSize, fontWeight).setVisible (true);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace ();
        }
    }
}
