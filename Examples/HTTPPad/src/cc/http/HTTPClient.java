/*
Copyright 2002-2014 by Capstone Courseware, LLC, and William W. Provost.
All rights reserved.
*/

package cc.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.net.Socket;

/**
Main engine for moving HTTP around.

@author Will Provost
*/
public class HTTPClient
{
    public static String DEFAULT_HOST =
        System.getProperty ("cc.http.defaultHost", "localhost");
    public static int DEFAULT_PORT = Integer.parseInt
        (System.getProperty ("cc.http.defaultPort", "8080"));

    private String host = DEFAULT_HOST;
    private int port = DEFAULT_PORT;
    private String root = "REPLACE_ME";
    private boolean forceRoot = false;
    private boolean filterHeader = true;
    private boolean sessionAware = true;
    private String sessionCookie;

    /**
    Default constructor, default values.
    */
    public HTTPClient ()
    {
    }

    /**
    Set a root URL which will override settings found in any scripts.
    */
    public HTTPClient (String root)
    {
        setRoot (root);
    }

    /**
    Accessor for host setting.
    */
    public String getHost ()
    {
        return host;
    }

    /**
    Mutator for host setting.
    */
    public void setHost (String newValue)
    {
        host = newValue;
    }

    /**
    Accessor for port setting.
    */
    public int getPort ()
    {
        return port;
    }

    /**
    Mutator for port setting.
    */
    public void setPort (int newValue)
    {
        port = newValue;
    }

    /**
    Accessor for root URL.
    */
    public String getRoot ()
    {
        return root;
    }

    /**
    Mutator for root URL.
    */
    public void setRoot (String newValue)
    {
        root = newValue;
        if (root != null)
            forceRoot = true;
        else
            root = "NO_ROOT_SET";
    }

    /**
    Accessor for filter setting.
    */
    public boolean isFilterHeader ()
    {
        return filterHeader;
    }

    /**
    Mutator for filter setting.
    */
    public void setFilterHeader (boolean newValue)
    {
        filterHeader = newValue;
    }

    /**
    Accessor for filter setting.
    */
    public boolean isSessionAware ()
    {
        return sessionAware;
    }

    /**
    Mutator for filter setting.
    */
    public void setSessionAware (boolean newValue)
    {
        sessionAware = newValue;
    }

    /**
    Helper method that passes or fails header entries according to filter
    criteria.
    */
    private static boolean passesFilter (String headerEntry)
    {
        final String[] headersWeLike = { "Content-Type" };

        for (String name : headersWeLike)
            if (headerEntry.length () > name.length () &&
                    headerEntry.substring (0, name.length () + 1)
                        .equalsIgnoreCase (name + ":"))
                return true;

        return false;
    }

    /**
    Convenience method to provide request content as a string;
    just delegates to {@link #invoke(java.io.Reader)}.
    */
    public String invoke (String requestString)
    {
        return invoke (new StringReader (requestString));
    }

    /**
    Parses the given character content as an HTTP request, getting the
    host and port from the Host header entry as it goes.
    Sets or replaces the Content-Length header entry based on actual body
    content, if any.
    Sends the request to the derived host and port, and returns the response
    verbatim, as a String.
    Closes initial reader when done.
    */
    public String invoke (Reader requestReader)
    {
        final String HOST = "Host:";
        final String CONTENT_LENGTH = "Content-Length:";
        final int HTTP_PORT = 80;
        final String END_LINE = "\r\n";

        StringBuilder request = new StringBuilder ();
        boolean specifiedHostAndPort = false;
        BufferedReader in = new BufferedReader (requestReader);

        try
        {
            // Pass all header entries except content-length.
            // Grab host and port on their way past.
            String line;
            while ((line = in.readLine ()) != null && line.length () != 0)
            {
                line = line.replace ("@HOST@", host)
                           .replace ("@PORT@", Integer.toString (port))
                           .replace ("@ROOT@", root);

                if (line.length () >= HOST.length () &&
                    line.substring (0, HOST.length ()).equalsIgnoreCase (HOST))
                {
                    String[] parts =
                        line.substring (HOST.length ()).trim ().split (":");
                    host = parts[0];
                    port = (parts.length > 1)
                        ? Integer.parseInt (parts[1])
                        : HTTP_PORT;
                    specifiedHostAndPort = true;
                }

                if (line.length () < CONTENT_LENGTH.length () ||
                    !line.substring (0, CONTENT_LENGTH.length ())
                        .equalsIgnoreCase (CONTENT_LENGTH))
                    request.append (line).append (END_LINE);
            }

            if (sessionCookie != null)
                request.append ("Cookie: JSESSIONID=")
                       .append (sessionCookie)
                       .append (END_LINE);

            if (!specifiedHostAndPort)
                request.append ("Host: ")
                       .append (host)
                       .append (":")
                       .append (port)
                       .append (END_LINE);

            if (line == null)
                request.append (END_LINE);
            else
            {
                // Buffer the body.
                StringBuilder body = new StringBuilder ();
                boolean first = true;
                while ((line = in.readLine ()) != null)
                {
                    if (!first)
                        body.append (END_LINE);
                    body.append (line);
                    first = true;
                }

                // Write content-length header entry, break, and body.
                request.append (CONTENT_LENGTH)
                       .append (" ")
                       .append (body.length ())
                       .append (END_LINE)
                       .append (END_LINE)
                       .append (body.toString ());
            }
        }
        catch (IOException ex)
        {
            ex.printStackTrace ();
            return "Unable to parse and send request.";
        }
        finally
        {
            try { in.close (); }
            catch (IOException ex) { ex.printStackTrace (); }
        }

        Socket socket = null;
        StringBuilder response = new StringBuilder ();
        try
        {
            socket = new Socket (host, port);
            PrintWriter out = new PrintWriter
                (socket.getOutputStream (), true);
            in = new BufferedReader
                (new InputStreamReader (socket.getInputStream ()));

            out.print (request.toString ());
            out.flush ();

            response.append (in.readLine ()).append ('\n');

            String line;
            int length = -1;
            boolean chunked = false;
            boolean binary = false;
            while ((line = in.readLine ()) != null)
            {
                if (line.length () == 0)
                {
                    response.append ('\n');
                    break;
                }

                if (!filterHeader || passesFilter (line))
                    response.append (line).append ('\n');

                if (line.length () >= 38 &&
                    line.substring (0, 38).equalsIgnoreCase
                        ("Content-Type: application/octet-stream"))
                    binary = true;

                if (line.length () >= 26 && line.substring (0, 26)
                        .equalsIgnoreCase ("Transfer-Encoding: chunked"))
                    chunked = true;

                if (line.length () > 14 &&
                    line.substring (0, 14).equalsIgnoreCase ("Content-Length"))
                    length = Integer.parseInt
                        (line.substring (16, line.length ()));

                if (line.length () >= 12 && line.substring (0, 12)
                    .equalsIgnoreCase ("Set-cookie: "))
                {
                    String[] cookies =
                        line.substring (12, line.length ()).split (";");
                    String[] nameAndValue = cookies[0].split ("=");
                    if (nameAndValue[0].equals ("JSESSIONID"))
                    {
                        sessionCookie = nameAndValue[1];
                    }
                }
            }

            int initialLength = response.length ();
            if (chunked || length >= 0)
            {
                if (chunked)
                    chunks: while ((line = in.readLine ()) != null)
                    {
                        String chunkSize = line.toLowerCase ();
                        int size = 0;
                        for (int c = 0; c < chunkSize.length (); ++c)
                        {
                            size *= 16;
                            char ch = chunkSize.charAt (c);
                            size += (ch >= '0' && ch <= '9')
                                ? (int) (ch - '0')
                                : (int) (ch - 'a' + 10);
                        }
                        if (line.length () == 0)
                            continue chunks;

                        if (size == 0)
                            break chunks;

                        for (int b = 0; b < size; ++b)
                            response.append ((char) in.read ());
                    }
                else if (length > 0)
                    for (int b = 0; b < length; ++b)
                    {
                        int r = in.read ();
                        if (binary && (r == 10 || r == 13))
                            r = '.';
                        response.append ((char) r);
                    }

                if (response.length () == initialLength)
                    response.append ("(No message body.)")
                            .append (System.getProperty ("line.separator"));
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace ();
        }
        finally
        {
            try { socket.close (); }
            catch (IOException ex) {}
        }

        return response.toString ();
    }

    /**
    Convenience method to provide request content as a string;
    just delegates to {@link #invokeScript(java.io.Reader)}.
    */
    public String invokeScript (String script)
    {
        return invokeScript (new StringReader (script));
    }

    /**
    Parses the given content as a script of possibly multiple HTTP requests,
    and sends each in sequence using {@link #invoke(java.io.Reader)}.
    We parse the script content as follows:
    <ul>
      <li>Everything is line-by-line: i.e. requests end with line breaks,
      and so does all other script content.</li>
      <li>Any line composed of only hyphens is a separator between other types of content.</li>
      <li>Any line composed of only equals signs is a separator, and indicates that what follows is a comment, to be ignored by the processor.</li>
      <li>Separators of either type are optional to start and end the script.</li>
    </ul>

    @return A compilation of responses, in a similar format to the
            request script
    */
    public String invokeScript (Reader script)
    {
        final String SEPARATOR_PATTERN = "\\-\\-.*";
        final String COMMENT_PATTERN = "==.*";
        final String END_LINE = "\r\n";
        final String SEPARATOR = "----";
        final String ECHO_PREFIX = "-- ";

        final String ECHO_ON_COMMAND = "-- ECHO ON";
        final String ECHO_OFF_COMMAND = "-- ECHO OFF";
        final String FILTER_ON_COMMAND = "-- FILTER ON";
        final String FILTER_OFF_COMMAND = "-- FILTER OFF";
        final String PAD_ON_COMMAND = "-- PAD ON";
        final String PAD_OFF_COMMAND = "-- PAD OFF";
        final String HOST_COMMAND = "-- HOST";
        final String PORT_COMMAND = "-- PORT";
        final String ROOT_COMMAND = "-- ROOT";

        BufferedReader in = new BufferedReader (script);
        String line;
        String firstLine = null;
        StringBuilder request = new StringBuilder ();
        StringBuilder responses = new StringBuilder ();
        boolean done = false;
        boolean echo = false;
        String pad = "";
        while (!done)
            try
            {
                boolean command = false;

                line = in.readLine ();
                if (line == null)
                {
                    line = SEPARATOR;
                    done = true;
                }
                else if (line.equalsIgnoreCase (ECHO_ON_COMMAND))
                {
                    command = true;
                    echo = true;
                }
                else if (line.equalsIgnoreCase (ECHO_OFF_COMMAND))
                {
                    command = true;
                    echo = false;
                }
                else if (line.equalsIgnoreCase (FILTER_ON_COMMAND))
                {
                    command = true;
                    filterHeader = true;
                }
                else if (line.equalsIgnoreCase (FILTER_OFF_COMMAND))
                {
                    command = true;
                    filterHeader = false;
                }
                else if (line.equalsIgnoreCase (PAD_ON_COMMAND))
                {
                    command = true;
                    pad = END_LINE;
                }
                else if (line.equalsIgnoreCase (PAD_OFF_COMMAND))
                {
                    command = true;
                    pad = "";
                }
                else if (line.startsWith (HOST_COMMAND))
                {
                    command = true;
                    host = line.substring (HOST_COMMAND.length () + 1);
                }
                else if (line.startsWith (PORT_COMMAND))
                {
                    command = true;
                    try
                    {
                        port = Integer.parseInt
                            (line.substring (PORT_COMMAND.length () + 1));
                    }
                    catch (NumberFormatException ex)
                    {
                        responses.append ("Couldn't parse port number.");
                    }
                }
                else if (line.startsWith (ROOT_COMMAND) && !forceRoot)
                {
                    command = true;
                    root = line.substring (HOST_COMMAND.length () + 1);
                }

                boolean separator = line.matches (SEPARATOR_PATTERN);
                boolean comment = line.matches (COMMENT_PATTERN);

                if (separator || comment)
                {
                    if (request != null && request.length () != 0)
                    {
                        if (echo)
                            responses.append (ECHO_PREFIX)
                                     .append (firstLine)
                                     .append (END_LINE);
                        responses.append (pad)
                                 .append (invoke (request.toString ()))
                                 .append (END_LINE)
                                 .append (pad);
                    }

                    if (!done && !command)
                        responses.append (line).append (END_LINE);

                    request = separator ? new StringBuilder () : null;
                    firstLine = null;
                }
                else if (request != null)
                {
                    request.append (line).append (END_LINE);
                    if (firstLine == null)
                        firstLine = line;
                }
                else
                    responses.append (line).append (END_LINE);
            }
            catch (IOException ex)
            {
                ex.printStackTrace ();
                responses.append
                    ("Caught exception trying to parse request script.")
                         .append (END_LINE);
            }

        try { in.close (); }
        catch (IOException ex) { ex.printStackTrace (); }

        return responses.toString ();
    }
}
