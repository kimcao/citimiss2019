/*
Copyright 2002-2014 by Capstone Courseware, LLC, and William W. Provost.
All rights reserved.
*/

package cc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
Footer for the main GUI.

@author Will Provost
*/
public class Footer
    extends JPanel
{
    public Footer ()
    {
        Font font = new Font ("Arial", Font.PLAIN, 9);
        Color color = new Color (49, 98, 49);

        JLabel capstone = new JLabel
            ("Copyright \u00A9 2004-2014 Capstone Courseware, LLC");
        capstone.setFont (font);
        capstone.setForeground (color);

        JLabel web = new JLabel ("www.capstonecourseware.com");
        web.setFont (font);
        web.setForeground (color);

        setLayout (new BorderLayout (12, 4));
        setBorder (BorderFactory.createEmptyBorder (4, 4, 4, 4));
        add (BorderLayout.WEST, capstone);
        add (BorderLayout.EAST, web);
    }
}
