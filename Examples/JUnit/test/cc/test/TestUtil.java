/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.test;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.stream.Collectors;

import org.junit.Assert;

/**
Utility for capturing expected test output and for comparing actual output
to expected.

@author Will Provost
*/
public class TestUtil
{
    private static final String ENDLINE = 
        System.getProperty ("line.separator");
    
    /**
    Writes a text file of the given name (or path) with the given contents.
    */
    public static void createFile (String filename, String contents)
        throws IOException
    {
        try
        (
            PrintWriter out = new PrintWriter (new FileWriter (filename));
        )
        {
            out.print (contents);
        }
    }
    
    /**
    Loads a file of the given name as a resource of the given class.
    */
    public static String contentsOf (Class<?> basis, String filename)
        throws IOException
    {
        try
        (
            BufferedReader in = new BufferedReader
                (new InputStreamReader 
                    (basis.getResourceAsStream (filename)));
        )
        {
            return in.lines ()
                .collect (Collectors.joining (ENDLINE)) + ENDLINE;
        }
    }

    /**
    Loads a file of the given name as a resource of the class
    of the given object.
    */
    public static String contentsOf (Object basis, String filename)
        throws IOException
    {
        return contentsOf (basis.getClass (), filename);
    }
    
    /**
    Loads a file of the given name as a resource of the given class,
    and JUnit-asserts that the given contents are the same as the 
    expected output contained in the file.
    */
    public static void assertEquals 
            (Class<?> basis, String filename, String contents)
        throws IOException
    {
        Assert.assertEquals (contentsOf (basis, filename), contents);
    }
    
    /**
    Loads a file of the given name as a resource of the class
    of the given object, and JUnit-asserts that the given contents 
    are the same as the  expected output contained in the file.
    */
    public static void assertEquals 
            (Object basis, String filename, String contents)
        throws IOException
    {
        Assert.assertEquals (contentsOf (basis, filename), contents);
    }
}
