/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import cc.cars.sales.Standard;
import cc.cars.sales.StandardTest;
import cc.test.TestUtil;

/**
Test for the {@link Buyer}.

@author Will Provost
*/
public class DealershipTest
{
    private Dealership dealership = new Dealership ();
    
    /**
    Test the output when listing cars against a prepared file.
    */
    @Test
    public void testList ()
        throws Exception
    {
        TestUtil.assertEquals 
            (this, "ListOutput.txt", dealership.listCars ());
    }
    
    /**
    Test finding a car by VIN.
    */
    @Test
    public void testFindByVINGood ()
        throws Exception
    {
        Car car = dealership.findCar ("ED9876");
        
        assertNotNull (car);
        assertEquals ("Toyota", car.getMake ());
        assertEquals ("Prius", car.getModel ());
        assertEquals (2015, car.getYear ());
    }

    /**
    Test finding a car by VIN, when there is no result.
    */
    @Test
    public void testFindByVINNoResult ()
        throws Exception
    {
        assertNull (dealership.findCar ("123456"));
    }

    /**
    Test finding a car by VIN, giving a null value for the VIN.
    */
    @Test
    public void testFindByVINBadInput ()
        throws Exception
    {
        assertNull (dealership.findCar (null));
    }
    
    /**
    Test finding a car by make and model.
    */
    @Test
    public void testFindByMakeAndModelGood ()
        throws Exception
    {
        Car car = dealership.findCar ("Subaru", "Outback");
        
        assertNotNull (car);
        assertEquals ("PV9228", car.getVIN ());
        assertEquals (2015, car.getYear ());
    }
    
    /**
    Test finding a car by make and model, when there is no result.
    */
    @Test
    public void testFindByMakeAndModelNoResult ()
        throws Exception
    {
        assertNull (dealership.findCar ("X", "Y"));
    }
    
    /**
    Test finding a car by make and model, giving null values for both.
    */
    @Test
    public void testFindByMakeAndModelBadInput ()
        throws Exception
    {
        assertNull (dealership.findCar (null, null));
    }
    
    /**
    Test selling a car, forcing the {@link cc.cars.sales.Standard}
    salesman type.
    */
    @Test
    public void testSellCarStandardSuccess ()
        throws Exception
    {
        System.setProperty 
            ("cc.cars.Salesman.type", Standard.class.getName ());
        Salesman salesman = 
            dealership.sellCar (StandardTest.getCarOfMyDreams ());
        StandardTest.assertSaleSuccess ((Standard) salesman);
    }
    
    /**
    Test inventory listing against a prepared file.
    */
    @Test
    public void testListInventory ()
        throws Exception
    {
        TestUtil.assertEquals 
            (this, "InventoryOutput.txt", dealership.listInventory ());
    }
}
