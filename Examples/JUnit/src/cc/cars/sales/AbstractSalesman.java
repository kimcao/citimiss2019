/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars.sales;

import cc.cars.Car;
import cc.cars.SaleHandler;
import cc.cars.Salesman;

/**
Base for common salesman behaviors.

@author Will Provost
*/
public abstract class AbstractSalesman
    implements Salesman
{
    private SaleHandler handler;
    protected Car car;

    /**
    Tell the salesman what car he's selling.
    */
    public AbstractSalesman (Car car)
    {
        this.car = car;
    }

    /**
    Register the given handler for possible callback.
    */
    public void onSale (SaleHandler handler)
    {
        this.handler = handler;
    }

    /**
    Mark the car sold, and notify any registered handler of the sale.
    */
    protected void closeSale ()
    {
        car.setSold (true);
        if (handler != null)
            handler.carSold (car);
    }
}
