/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

/**
Comparison of methods of creating mock objects.

@author Will Provost
*/
public class ListTest
{
    /**
    Test for a list, expecting it to to have exactly two specific elements.
    */
    public void testList (List<String> list)
        throws Exception
    {
        assertEquals (2, list.size ());
        assertEquals ("one", list.get (0));
        assertEquals ("two", list.get (1));
    }

    /**
    Test a prepared ArrayList.
    */
    @Test
    public void testRealList ()
        throws Exception
    {
        List<String> list = new ArrayList<> ();
        list.add ("one");
        list.add ("two");
        testList (list);
    }

    /**
    Test a Mockito-mocked list.
    */
    @Test
    @SuppressWarnings ("unchecked") // mocking a generic interface
    public void testMockitoList ()
        throws Exception
    {
        List<String> list = Mockito.mock (List.class);
        Mockito.when (list.size ()).thenReturn (2);
        Mockito.when (list.get (0)).thenReturn ("one");
        Mockito.when (list.get (1)).thenReturn ("two");
        testList (list);
    }
}
