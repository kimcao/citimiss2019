/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;

/**
Test for the {@link Fibonacci} class.

@author Will Provost
*/
public class FibonacciTest
{
    private Fibonacci fibonacci = new Fibonacci ();

    /**
    Test by registering a mock handler, and then verifying expected calls,
    one-by-one.
    */
    @Test
    public void testOneAtATime ()
        throws Exception
    {
        BoundedSequence consumer = Mockito.mock (BoundedSequence.class);
        fibonacci.setHandler (consumer);
        fibonacci.run (10);

        Mockito.verify (consumer).start ();
        Mockito.verify (consumer, Mockito.times (2)).next (1);
        Mockito.verify (consumer).next (2);
        Mockito.verify (consumer).next (3);
        Mockito.verify (consumer).next (5);
        Mockito.verify (consumer).next (8);
        Mockito.verify (consumer).next (13);
        Mockito.verify (consumer).next (21);
        Mockito.verify (consumer).next (34);
        Mockito.verify (consumer).next (55);
        Mockito.verify (consumer).end ();
    }

    /**
    Test by registering a mock handler, and then verifying calls.
    Verify the number callbacks all together, with an argument captor,
    and then assert the right values over that captor's full value set.
    */
    @Test
    public void testWithCaptor ()
        throws Exception
    {
        final int[] sequence = { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 };

        ArgumentCaptor<Integer> captor =
            ArgumentCaptor.forClass (Integer.class);
        BoundedSequence consumer = Mockito.mock (BoundedSequence.class);
        fibonacci.setHandler (consumer);
        fibonacci.run (sequence.length);

        Mockito.verify (consumer).start ();
        Mockito.verify (consumer, Mockito.times (sequence.length))
            .next (captor.capture ());
        Mockito.verify (consumer).end ();

        int index = 0;
        for (int number : captor.getAllValues ())
            assertEquals (sequence[index++], number);
    }

    /**
    Test by registering a mock handler and then verifying all calls.
    Verify the number callbacks using a custom argument matcher that
    expects a different number each time, as per the configured sequence.
    */
    @Test
    public void testWithMatcher ()
        throws Exception
    {
        final int[] sequence = { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 };

        class Sequencer
            extends ArgumentMatcher<Integer>
        {
            private int index = 0;

            @Override
            public boolean matches (Object argument)
            {
                return sequence[index++ % 10] == ((Integer) argument).intValue ();
            }
        }

        BoundedSequence consumer = Mockito.mock (BoundedSequence.class);
        fibonacci.setHandler (consumer);
        fibonacci.run (sequence.length);

        Mockito.verify (consumer).start ();
        Mockito.verify (consumer, Mockito.times (sequence.length))
            .next (Mockito.intThat (new Sequencer ()));
        Mockito.verify (consumer).end ();
    }
}
