/*
Copyright 2004-2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars;

import static org.junit.Assert.*;
import org.junit.Test;

/**
Test for the {@link Car} class.

@author Will Provost
*/
public class CarTest
{
    /**
    Run the car through initialization and check all get methods.
    */
    @Test
    public void testConstructorAndGetters ()
        throws Exception
    {
        Car target = new Car ("Toyota", "Prius", 2002, "TY7890", 
            "Silver", 21999.99, 210, 220, Car.Handling.FAIR);
            
        assertEquals ("Toyota", target.getMake ());
        assertEquals ("Prius", target.getModel ());
        assertEquals (2002, target.getYear ());
        assertEquals ("TY7890", target.getVIN ());
        assertEquals ("Silver", target.getColor ());
        assertEquals (21999.99, target.getPrice (), 0.0001);
        assertEquals (21999.99, 
            target.testDrive ().getPerceivedValue (), 0.0001);
        assertFalse (target.isSold ());

        assertEquals ("2002 Toyota Prius", target.getName ());
        assertEquals (1, target.getQuantity ());
        assertEquals (21999.99, target.getPrice (), 0.0001);
    }
    
    /**
    Check behavior as inventory item.
    */
    @Test
    public void testSetSold ()
        throws Exception
    {
        Car target = new Car ("Toyota", "Prius", 2002, "TY7890", 
            "Silver", 21999.99, 210, 220, Car.Handling.FAIR);
        target.setSold (true);
        
        assertTrue (target.isSold ());
        assertEquals ("2002 Toyota Prius", target.getName ());
        assertEquals (0, target.getQuantity ());
        assertEquals (21999.99, target.getPrice (), 0.0001);
    }
}

