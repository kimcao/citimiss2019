To set up a local Jenkins server:

Create a folder on your Windows VM, anywhere you like, and into it download the Jenkins WAR deployment. Get the file here ...

    http://mirrors.jenkins.io/war-stable/latest/jenkins.war

... or, if this doesn't work, directly, go to the following URL instead, and navigate to the link for the "Generic Java package."

    https://jenkins.io/download/

From a command console, run the following command:

    java -jar jenkins.war --httpPort=9080

The exact port setting could vary; it's necessary on our machines because Jenkins's default port 8080 is already occupied.

In the console, you'll see a banner advising you that initial setup is required, and it will show a generated administrator password. Copy this from the console.

Now open a browser at the following URL:

    http://localhost:9080

Paste the password into the HTTP form field, and click **Submit**. Then choose to **Install suggested plugins.** This will take a couple of minutes.

In the next screen that comes up, enter a username and password for yourself as the server administrator.Click **Save and Continue**, then **Save and Finish**, and finally **Start using Jenkins**.



