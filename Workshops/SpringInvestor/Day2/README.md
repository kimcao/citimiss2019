# Spring Boot Workshop

Today we'll continue working on our SpringInvestor application, adding a Spring Boot application and a REST web service that will allow remote HTTP access to investors, which we will also start calling "accounts."

## Criteria for Completed Work

Your project must be available on a public BitBucket repository, the URL of which you'll provide to the instructors for review. Implement as much of the exercise as you can, and whatever you have implemented must compile cleanly. The Maven 'test' goal must run cleanly as well.

You will also demonstrate a working Jenkins project that can run all tests on demand.

Only if your project's repository URL has changed since yesterday, submit your work using this [Google form](https://docs.google.com/forms/d/e/1FAIpQLSf7l_DyhyOzZNIm7E1YqwglNLQECtXQaRF58IECugWuSQi1ig/viewform).

## Part 1. **BrokerService**

Create a new Java class that holds a map of **Investor** objects, with the ID of each investor as the key and the whole object as the value. Implement it to support at least the following operations:

    public List<Investor> getAll();
    public Investor getByID(int ID);
    public Investor openAccount(double cash);
    public void closeAccount(int ID);

You will want to make the class a Spring @Component, and give it an **@****Autowired ApplicationContext** field. This will allow you to create new **Investor** instances as needed, by calling **getBean()** on the context of which your object is a member.

Give the class a **@****PostConstruct** method that adds a couple of investors to the brokerage, just to make testing easier and more intesting. Give each a unique ID, of course, and set each up with a small portfolio of stocks and some cash.

Implement a basic unit test for your class -- at least one test case for each of the four methods above, and more as you see fit.

## Part 2. **BrokerApplication**

1. Add an **ID** field to **Investor**, of type **int**, and build getter and setter methods for it.

1. Create a class **BrokerApplication** that is a **@****SpringBootApplication**.

1. Make it a **@****PropertySource**, loading from **application.properties**.

3. Give it a **main()** method that calls **SpringApplication.run()**, passing the class itself and the command-line arguments.

4. Add a custom port setting to  your properties file:

    server.port=8082

## Part 3. Make it a Web Service

Now make your class ready to serve as a REST web service:

1. Replace your **spring-context** dependency with **spring-boot-starter-web**.

1. Change the **@****Component** annotation to **@****RestController**.

1. Add a **@****RequestMapping** annotation to the class, with the value ("accounts").

1. Add a **@****GetMapping** to the **getAll()** method, and clarify that the method produces a JSON response.

1. Run **BrokerApplication** as a Java application. You should see the application take a few seconds to start up, and then it will sit running, waiting for HTTP requests.

1. Test your first operation in a browser, at the following URL, and you should see a JSON reporesentation of the two built-in objects:

    http://localhost:8082/accounts

1. Annotate and test each of the remaining service methods:

    * **getByID()** is another HTTP GET, but at a subpath that can be anything and is bound to the method's **ID** parameter. (For example a request might be http://localhost:8082/accounts/2.)

    * **openAccount()** is an HTTP POST. It should return HTTP 201 Created, and the **cash** parameter must be bound to a parameter of the same name on the HTTP request.

    * **closeAccount()** is an HTTP DELETE. Like **getByID()** this operation is available at a subpath whose value is bound to the **ID** parameter.

## Part 4. Error Handling

1. Change the return type of **getByID()** from **Investor** to **ResponseEntity<Investor>**.

1. Return a new **ResponseEntity**, passing the investor object that you used to return directly, along with **HttpStatus.OK**.

1. Now, make it so this return happens only if the map has a key matching the requested ID. If there is no such key, instead return an object built just with **HttpStatus.NOT_FOUND**.

1. Test this in your browser by requesting both existing and non-existing IDs.

1. Make similar changes to **closeAccount()**.


## Stretch Goals

1. Set up a project on one of your team's Jenkins servers that will run **mvn test**, and make sure it runs cleanly. You may find that you need to exclude integration tests to get a clean build -- do so! You will need this in place for tomorrow's deployment work.

1. Develop a second unit test that uses **MockMvc** to prove out the REST annotations on **BrokerService**. This can be __included__ in your Maven/Jenknis tests.

1. Add operations that allow a remote caller to request stock trades for a specific investor. Think about the HTTP API for this, and discuss with the instructor before implementing. Consider error-reporting options, too. Add coverage of your new operations to your existing test(s).


