# Devops Workshop

Today you will complete your Spring Boot web service and deploy it to an OpenShift development environment. Any viable application that can accept and answer HTTP requests will be a good starting point for the tasks below; if you have time after deploying the application, you might go back and refine the application itself, and redploy through the pipeline as you go.

## Criteria for Completed Work

Your completed project must be fully represented in your BitBucket repository: the Maven project structure you've been working with so far, along with all of the Docker and OpenShift configuration files and any supporting resources. In short, it should be possible to deploy your application to __any__ OpenShift server by applying the **jenkins-pipeline.yaml** file in your repository to a new project on that server -- with no fixups, no workarounds, just a portable, deployable application.

Submit your name, teammate names, final BitBucket repository URL, and any notes using the [Google form](https://docs.google.com/forms/d/e/1FAIpQLSf7l_DyhyOzZNIm7E1YqwglNLQECtXQaRF58IECugWuSQi1ig/viewform).

## Tasks

For many of the following steps there are template files to help you get started. Look for replaceable parameters in these files, set off with **@****@** characters before and after a descriptive token, such as **@****@**your-app**@****@**.

### Part 1. Docker

1. Build a Docker image around your web service, and prove it out by starting a Docker container and hitting the service via **curl** at a local port. There is a template for your docker file that sets up a plain-vanilla Linux/Java environment; you take it from there!

1. Build a **docker-compose.yaml** to wrap that image as a single-module Docker service, and prove that the composed service can be started, tested, and stopped as well.

### Part 2. OpenShift

Build a Jenkins pipeline, with supporting deployment and route configurations, and set up a pipeline build on the OpenShift development server. Resources, possible steps, and tips follow:

* Four templates are available as starting points for your work: **jenkins-pipeline.yaml**, **deploymentconfig.yaml**, **route.yaml**, and **service.yaml**. __You must edit these files to use a unique name for your application:__ replace the token **@****@**your-app**@****@** in all files with a name that is unique to your team. Remember that this name can only include letters, digits, and the hyphen character; don't use underscores as they will run \afoul of OpenShift's need to develop a DNS name for the deployed application.

* Key commands to invoke on your Linux machines, once yo uhave viable Docker files and have worked over the supplied configuration files, are:

    oc login -u __username__ https://dev1.conygre.com:8443
    oc new-project __@@your-app@@__
    oc apply -f jenkins-pipeline.yaml

This last one will kick off a process of setting up an OpenShift pipeline build that can take a few minutes. Log in to https://dev1.conygre.com:8443 in a browser, and be sure that the project and build pipeline are completely initialized. You will need to set up the pipeline to run on the Jenkins server embedded in OpenShift, which involves logging on to Jenkins and granting authorization to OpenShift to trigger and monitor builds.

* Once everything is set, either run this command from your Linux machine ...

    oc start-build __@@your-app@@__

... trigger a build from the web console. This too will take some time. You can monitor the build process, including console output, as it runs, and address any issues before kicking off a fresh build.

* Any change to the pipeline YAML must be re-applied with **oc apply**. Any change to other files must be pushed to your repository. Then trigger a new build.

* Remember that, in the pipeline build, Jenkins will be starting fresh with a clone of your BitBucket repository. Anything you need it to know -- any file you reference in the pipeline YAML, or in any file that you reference, including the original Docker configurations, must be in the rpository and must be referenced by a relative path in or below the pipeline-YAML's location.

* It's not a multi-day approval process or a ten-hour automated build, but deploying an application through to an OpenShift server is still an exercise in patience. Setting up a pipeline can take several minutes, including some interactive work but a lot of it is just waiting for work to complete. Use the web interface to keep an eye on pipeline creation, build status, and eventually the deployed application.

### Stretch Goal

When your application is deployed, and you can send it requests at __@@your-app@@__.dev1.conygre.com, add some important administrative features:

1. Set resource lmits: reasonable requested and maximum allocations for memory and CPU usage.

1. A "readiness probe" checks that a newly-deployed application is healthy enough to go live; a "liveness probe" checks that a live application is still running, and if this check fails the container can be re-started.

All of these features can be developed using the OpenShift web console: look under **Applications / Deployments**, choose your running application, and then find options under the **Actions** menu in the upper right-hand corner of the web page. Once you apply any of these changes -- wait for the resulting re-deployment to complete -- you can view/edit the updated YAML file. Find the appropriate secions, copy them back into your **jenkins-pipeline.yaml**, and re-apply, so that you know you can repeat the deployment process and have these features in place from now on.


