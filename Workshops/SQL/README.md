# SQL Workshop

Today we will explore two data sets, developing progressively more complex queries and data-analysis processes. Complete as many of the challenges described below as you can during the day, and submit your completed work for review.

Both of these exercises are designed for Microsoft SQLServer. Start the SQLServer Manager on your Windows workstation. You can work with each of these schema in the temporary database, or create new databases for each of them. The table names don't collide, so you can install both and work on them in any order.

## Criteria for Completed Work

Your work product will be a single SQL script for each of the two exercises - that is, one for the bonds schema and one for the trades schema. Follow this naming convention for your files:

    bonds_MyName.sql
    trades_MyName.sql

It must be possible to run the following scripts, in the following order, in a new or cleaned-out SQLServer database. **Test this before you submit your work:**

    create_bonds_db.sql
    bonds_MyName.sql

    create_trades_db.sql
    populate_trades_db.sql
    trades_MyName.sql

## Bond Portfolio

Get the SQL script [create\_bonds\_db.sql](create_bonds_db.sql) and run it in SQLServer to create and populate a small data set of bond holdings. Explore the two-table schema and see that the rating of a bond carries additional information, linked in from a second table.

Develop a script that will provide the following information:

1. Show all information about the bond with the CUSIP '28717RH95'.

2. Show all information about all bonds, in order from shortest (earliest maturity) to longest (latest maturity).

3. Calculate the value of this bond portfolio, as the sum of the product of each bond's quantity and price.

4. Show the annual return for each bond, as the product of the quantity and the coupon. Note that the coupon rates are quoted as whole percentage points, so to use them here you will divide their values by 100.

5. Show bonds only of a certain quality and above, for example those bonds with ratings at least AA2. (Don't resort to regular-expression or other string-matching tricks; use the bond-rating ordinal.)

6. Show the average price and coupon rate for all bonds of each bond rating.

7. Calculate the yield for each bond, as the ratio of coupon to price. Then, identify bonds that we might consider to be overpriced, as those whose yield is less than the expected yield given the rating of the bond.

## Trading History

Get the two scripts [create\_trades\_db.sql](create_trades_db.sql) and [populate\_trades\_db.sql](populate_trades_db.sql) and run them in SQLServer. The data here represents the one-day trading history of three individual stock traders. For each trader there are many positions, and each position has an opening trade and most have a closing trade; some positions were left open at the end of the day. There is also a free-standing table of the pricing data that was available to the traders over the day, in 5-minute periods each with open, high, low, and closing prices, along with trading volume.

Develop a script that will provide the following information:

1. Find all trades by a given trader on a given stock - for example the trader with ID=1 and the ticker 'MRK'. This involves joining from trader to position and then to trades twice, through the opening- and closing-trade FKs.

2. Find the total profit or loss for a given trader over the day, as the sum of the product of trade size and price for all sales, minus the sum of the product of size and price for all buys.

3. Develop a view that shows profit or loss for all traders.

## Stretch Goals

1. In the bonds schema, show the degree of correlation between the bond rating and the coupon rate. Use the ordinal ranking as a surrogate for the bond rating. This is a bit of work, and especially note that it is best developed not as a single query, but by defining a view and then querying from that. Here is a reference for the process of calculating the [Pearson correlation coefficient](https://study.com/academy/lesson/pearson-correlation-coefficient-formula-example-significance.html).

2. See what sort of analysis you can provide a trader on their trading history. Did they buy or sell a little too early, a little too late? It won't be predictive, exactly, but it might be illuminating to the trader to see where they could have, for example, held a stock a little longer instead of selling for a quicker but smaller profit. A possible strategy for this is as follows:
    * Find the price points that relate to each trade, as those with the same stock symbol and a timestamp within some number of minutes of the actual trade time.
    * Of those price points, find the maximum (for sells) or minimum (for buys)
    * Produce a row with the original trade information, along with the best-alternative price and time that you've just found.
    * You could derive information from there, such as how much money was "left on the table," or perhaps find patterns of early or late buying or selling - find the most common time gap between actual trade and optimal trading time.
