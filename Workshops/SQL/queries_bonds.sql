-- 1. Find a specific bond by CUSIP

select *
from bond
where CUSIP='28717RH95';


-- 2. Show all ordering e.g. by coupon rate, descending

select *
from bond
order by coupon desc;


-- 3. Calculate portfolio value

select sum(quantity * price)
from bond;


-- 4. Calculate annual return

select sum(quantity * coupon / 100)
from bond;


-- 5A. Get holdings with AA2 rating and better
--   using a join

select b.CUSIP, b.rating
from bond b
  join rating r on b.rating = r.rating
where r.ordinal < 4
order by r.ordinal;

-- 5B. Get holdings with AA2 rating and better
--   using a subquery; not as nice b/c can't order by ordinal

select CUSIP, rating from bond where rating in
  (select rating from rating where ordinal < 4)
order by rating;


-- 6. Show average price and coupon by rating, ordering by quality(ordinal)

select r.rating, sum(b.quantity * b.coupon / 100) as 'return'
from bond b
  join rating r on b.rating = r.rating
group by r.ordinal, r.rating
order by r.ordinal;


-- 7. Calculate yield for each bond, and identify overpriced bonds

select b.CUSIP, b.coupon / b.price as yield, r.expected_yield
from bond b
  join rating r on b.rating = r.rating
where b.coupon / b.price < r.expected_yield;


-- Stretch goal: correlation coefficient

create view products as
select b.coupon as x,
  r.ordinal as y,
  b.coupon * r.ordinal as product,
  b.coupon * b.coupon as x_square,
  r.ordinal * r.ordinal as y_square
from bond b
  join rating r on b.rating = r.rating;

select (count(*) * sum(product) - sum(x) * sum(y)) /
  sqrt((count(*) * sum(x_square) - sum(x) * sum(x)) *
       (count(*) * sum(y_square) - sum(y) * sum(y)))
from products;

